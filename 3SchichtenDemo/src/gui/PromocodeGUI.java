package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;
import java.awt.Color;
import javax.swing.BoxLayout;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblPromocode;

	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setTitle("PromotionCodeGenerator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 200);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		lblPromocode = new JLabel("");
		lblPromocode.setBackground(Color.WHITE);
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setFont(new Font("Calibri",Font.PLAIN, 18));
		contentPane.add(lblPromocode);
		
		JButton btnNewPromoCode = new JButton("Generiere neuen Promotioncode");
		btnNewPromoCode.setBackground(Color.GRAY);
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code = logic.getNewPromoCode();
				lblPromocode.setText(code);
			}
		});
		contentPane.add(btnNewPromoCode, BorderLayout.NORTH);
		
		JLabel lblBeschriftungPromocode = new JLabel("Promotioncode fuer 2-fach IT$ in einer Stunde");
		lblBeschriftungPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblBeschriftungPromocode.setFont(new Font("Dialog", Font.PLAIN, 16));
		contentPane.add(lblBeschriftungPromocode, BorderLayout.SOUTH);
		this.setVisible(true);
	}

}
