package data;

import java.util.LinkedList;

import java.util.List;
import java.io.*;


public class PromoCodeData implements IPromoCodeData {

	private List<String> promocodes;
	File file = new File("Ausgabe.txt"); //Datei, in die geschrieben werden soll

	public PromoCodeData(){
		this.promocodes = new LinkedList<String>();
	}
	
	@Override
	public boolean savePromoCode(String code) {
		
		File file = new File("Ausgabe.txt"); //Datei, in die geschrieben werden soll

		try {
		   BufferedWriter writer = new BufferedWriter(new FileWriter(file,true)); //Erzeugen eines effizienten Writers für Textdateien
		   writer.write(code);
		   writer.newLine();
		   writer.close();
		}
		catch(IOException ioe) {
		   System.err.println(ioe);
		}
		return this.promocodes.add(code);
		
	
	
	}
	

	@Override
	public boolean isPromoCode(String code) {
		return this.promocodes.contains(code);
	}

}
